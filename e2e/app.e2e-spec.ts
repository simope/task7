import { Task7Page } from './app.po';

describe('task7 App', function() {
  let page: Task7Page;

  beforeEach(() => {
    page = new Task7Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
